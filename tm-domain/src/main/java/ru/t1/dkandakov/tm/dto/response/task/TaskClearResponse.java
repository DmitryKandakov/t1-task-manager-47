package ru.t1.dkandakov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import ru.t1.dkandakov.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class TaskClearResponse extends AbstractResponse {
}
