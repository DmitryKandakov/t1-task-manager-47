package ru.t1.dkandakov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.dkandakov.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {
}
