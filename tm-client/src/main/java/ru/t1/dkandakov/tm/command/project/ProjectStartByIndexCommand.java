package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(getToken(), index, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by index.";
    }

}